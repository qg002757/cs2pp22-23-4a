# IPython
https://ipython.readthedocs.io/en/stable/interactive/tutorial.html
# Matplotlib
Matplotlib is an amazing visualization library in python for 2D plots of arrays.
https://www.geeksforgeeks.org/python-introduction-matplotlib/
## plot method - Essentially allows us to plot a graph
https://www.geeksforgeeks.org/matplotlib-pyplot-plot-function-in-python/
## asarray() - This function is used when we want to convert input into an array. Input can be lists, lists of tuples, tuples, tuples of tuples, tuples of lists and arrays
https://www.geeksforgeeks.org/numpy-asarray-in-python/
## matplotlib.pyplot.hlines - Used to draw a horizontal line in a graph at each y from xmin to xmax
## legend - Essentially allows us to give lable to the different graphs
https://www.geeksforgeeks.org/matplotlib-pyplot-legend-in-python/
## Frozenset - Essentially is just used to create an iummutab;e set from an iterable
https://www.geeksforgeeks.org/frozenset-in-python/
## bytes - Used to convert an object to a immutable byte-represented object of a given size and data
https://www.geeksforgeeks.org/python-bytes-method/
## bytearray
##memoryview
## factorial
